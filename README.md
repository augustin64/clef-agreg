# Pronaos

https://agreg-info.gitlab.io/docs/

Pronaos est un environnement de concours pour permettre aux candidats de l'agrégation d'informatique de préparer leurs épreuves orales.

La version 2022 est disponible au format OVA pour VirtualBox [ici](https://gitlab.com/agreg-info/clef-agreg/-/releases). L'image est basée sur Ubuntu avec un ensemble spécifique de paquets qui seront mis à disposition des candidats lors des épreuves.

Le compte `candidat` a pour mot de passe `concours`.

## Création de la VM

[`cloud-config.yaml`](cloud-config.yaml) contient notamment les packages à installer et les scripts de génération de l'image suivant le standard [cloud-init](https://cloudinit.readthedocs.io/en/latest/index.html).

[`requirements.txt`](requirements.txt) contient les packages Python à installer.

Le processus de création de VM prend environ 2 heures. La machine virtuelle sera disponible sur agreg-info.org.

Si toutefois vous souhaitez l'installer vous-mêmes, suivez les instructions du test d'intégration continue :

- [`.gitlab-ci.yml`](.gitlab-ci.yml) lance les scripts ci-dessous et, en cas de succès, uploade la machine virtuelle sur un serveur
- [`scripts/build.sh`](scripts/build.sh) crée l'image `.img` et la machine virtuelle `.vdi`
- [`scripts/install.sh`](scripts/install.sh) finalise l'installation et la configuration des derniers paquets, une fois ce repo cloné (celui dont vous êtes en train de lire le README).

## Différences avec l'environnement de concours

Le compte `candidat` n'existera pas pendant les oraux, chaque candidat aura un compte spécifique.

Le compte `candidat` est administrateur de la VM, ce qui ne sera pas le cas pendant les oraux.

La VM a un accès réseau non restreint (en particulier, a accès à internet). Pendant le concours, les machines n'auront qu'un accès à un intranet local.
