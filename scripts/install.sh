#!/bin/bash

# custom (non-ppa) installations
# launched automatically from runcmd
# $pwd is clef-agreg/

# SYSTEM CONFIG

# Python packages
# python3 -m pip install -r requirements.txt
# pip freeze -r requirements.txt > /home/candidat/packages.txt

# Enable phpmymadmin
# ln -s /usr/share/phpmyadmin/ /var/www/html
# mysql -e "create user candidat identified by 'concours'"
# mysql -e "grant all privileges on *.* to candidat with grant option"
# mysql -e "flush privileges"

# Network bugs
echo "" > /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf

# Install Filius
# wget https://www.lernsoftware-filius.de/downloads/Setup/filius_1.13.2_all.deb
# apt install ./filius_1.13.2_all.deb

# Install ns-3
# wget https://www.nsnam.org/releases/ns-allinone-3.35.tar.bz2
# tar -xf ns-allinone-3.35.tar.bz2
# rm ns-allinone-3.35.tar.bz2
# mv ns-allinone-3.35 /opt/

# Scratch
# wget http://www.ac-grenoble.fr/maths/scratch/scratch.zip
# unzip scratch.zip -d /home/candidat/scratch

# PyCharm
# snap install pycharm-community --classic

# Locale
localectl set-locale fr_FR.UTF-8
localectl set-x11-keymap fr

# Cleanup
apt autoclean

# USER CONFIG
# VSCode directories and clipit annoying startup question
mkdir -p /home/candidat/.config/VSCodium
mkdir -p /home/candidat/.vscode-oss/extensions
cp -r config/* /home/candidat/.config
# Tuareg mode
# mkdir -p /home/candidat/.emacs.d && cd /home/candidat/.emacs.d && git clone https://github.com/ocaml/tuareg && cd tuareg && make
# echo '(load "/home/candidat/.emacs.d/tuareg/tuareg-site-file")' >> /home/candidat/.emacs
chown -R candidat:candidat /home/candidat

su candidat -c "opam init -a"
su candidat -c "opam install -y utop"
# su candidat -c "eval \$(opam env) && ocaml-jupyter-opam-genspec"
su candidat -c "echo 'eval \$(opam env)' >> /home/candidat/.bashrc"
# su candidat -c "jupyter kernelspec install --user --name ocaml-jupyter /home/candidat/.opam/default/share/jupyter"
su candidat -c "codium --install-extension ms-ceintl.vscode-language-pack-fr"
su candidat -c "codium --install-extension ocamllabs.ocaml-platform"
#su candidat -c "codium --install-extension ms-python.python"
# ms-vscode.cpptools not available on open-vsx.org used by codium (as using regular marketplace is not legal in a fork of VSCode)
su candidat -c 'curl -s https://api.github.com/repos/microsoft/vscode-cpptools/releases/latest \
                | grep cpptools-linux.vsix \
                | cut -d : -f 2,3 \
                | tr -d \" \
                | wget -qi -'
su candidat -c "codium --install-extension cpptools-linux.vsix"
su candidat -c "rm cpptools-linux.vsix"

rm -rf /var/cache/apt
rm -rf /var/lib/snapd/snaps

# Remove sudo rights to candidat
sudo rm /etc/sudoers.d/90-cloud-init-users
